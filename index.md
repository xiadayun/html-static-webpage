<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>测试网页</title>
    <style>  
        .my-button {  
          background-color: #4CAF50; /* 绿色背景 */  
          border: none; /* 无边框 */  
          color: white; /* 白色文本 */  
          padding: 15px 32px; /* 内边距 */  
          text-align: center; /* 文本居中 */  
          text-decoration: none; /* 无下划线 */  
          display: inline-block; /* 行内块元素 */  
          font-size: 16px; /* 字体大小 */  
          margin: 4px 2px; /* 外边距 */  
          cursor: pointer; /* 鼠标悬停时显示手形光标 */  
        }  
      </style> 
</head>
<body>
    <h1>测试用的</h1>
    <p>后面可以加点内容进来</p>
    <button class="my-button" onclick="hhh()" >点击我</button>
    <input type="text">

</body>
<script>
    function hhh(){
        alert("你点击了按钮");
    }
</script>
</html>
